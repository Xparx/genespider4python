import numpy as np
from numpy import dot
from numpy import transpose as tp
from numpy.linalg import lstsq


class matcls():

    def __init__(self, Ainit, Y, P, R, verbose=False, alpha=0.01, beta=0.5, prec=1e-6, max_steps=100, max_bt_steps=100):
        self.v = verbose
        self.alpha = alpha
        self.beta = beta
        self.prec = prec
        self.max_steps = max_steps
        self.max_bt_steps = max_bt_steps
        self.n = Ainit.shape[0]
        self.m = Ainit.shape[1]
        self.d = self.n * self.m
        self.xhat = np.reshape(Ainit, self.d, order="F")
        self.Y = Y.copy()
        self.P = P.copy()
        self.R = R.copy()

        self.status = 'Failed'
        tol = np.finfo(float).eps
        astruct = (np.abs(Ainit) > tol).astype(int)
        self.k = np.sum(astruct)
        if self.k == 0:
            self.status = "Solved"
            self.results = Ainit

            if verbose:
                print("Zero networks are trivial. Completed with status = " + self.status)

        self.z = np.zeros(self.k)  # optimization variable

        self.independent = True
        if self.Y.shape[0] > self.Y.shape[1]:
            self.independent = False

        self.affine()
        self.presets()
        self.hessian()

    def x(self, z):
        return dot(self.F, z) + self.xhat

    def A(self, x):
        return np.reshape(x, [self.n, self.m], order="F")

    def g(self, A):
        return dot(tp(self.R), dot(A, self.Y)) + dot(tp(self.R), self.P)

    def f(self, A):
        gx = self.g(A)
        return np.trace(dot(gx, tp(gx)))

    def presets(self):
        self.RRt = dot(self.R, tp(self.R))
        self.YYt = dot(self.Y, tp(self.Y))
        self.gterm2 = 2 * dot(self.RRt, dot(self.P, tp(self.Y)))

    def grad(self, A):
        J = 2 * dot(self.RRt, dot(A, self.YYt)) + self.gterm2
        return np.reshape(J, self.d, order="F")

    def affine(self):
        F = np.zeros([self.d, self.k])
        tol = np.finfo(float).eps
        nonzero = np.abs(self.xhat) > tol
        ix = 0
        for i in range(self.d):
            if nonzero[i]:
                F[i, ix] = 1.0
                ix += 1

        self.F = F

    def hessian(self):
        '''Hessian is constant'''

        H = dot(tp(self.F), dot(np.kron(self.YYt, 2 * self.RRt), self.F))
        if self.independent:
            H_L = np.linalg.cholesky(H)
            # H_L = tp(H_L)  # A = H_L * tp(H_L)
            H_out = H_L
        else:  # Should be positive definite
            H_out = np.linalg.pinv(H)  # pseudoinverse

        self.H = H_out

    def line_search(self, znt, g, fx):

        # z = np.zeros(self.F.shape[1])
        t = 1 / self.beta
        lhs = np.infty
        rhs = np.infty
        btx = 0

        while True:
            t = self.beta * t
            zcurrent = self.z + t * znt
            xcurr = dot(self.F, zcurrent) + self.xhat
            # A = self.reshape(xcurr, self.n, self.m, order="F")

            lhs = self.f(self.A(xcurr))

            rhs = fx + self.alpha * t * dot(g, znt)

            # print(t)
            # print((lhs < rhs))
            if (lhs < rhs).all():
                self.z = self.z + t * znt
                return

            if btx > self.max_bt_steps:
                raise StopIteration('Short-circuited line search')

    def update(self, i):

        A = self.A(self.x(self.z))
        fx = self.f(A)
        g = dot(tp(self.F), self.grad(A))

        if self.independent:
            znt = -lstsq(tp(self.H), lstsq(self.H, g)[0])[0]
        else:
            znt = -dot(self.H, g)

        gap = -0.5 * dot(g, znt)

        # stopping
        if gap < self.prec:
            self.status = "Solved"
            self.results = A

        self.line_search(znt, g, fx)

    def verbose(self, i=None):
        frob = np.linalg.norm(dot(dot(self.A(self.x(self.z)), self.Y) + self.P, self.R), "fro")

        if i is None:
            print(" step\t value\t gap\t\t")
        else:
            print("  %5d\t" % (i,))

    def cls(self):

        # x = self.xhat  # warm start

        for i in range(self.max_steps):
            self.update(i)
            if self.status == "Solved":
                break


def main():
    data, net = example_data()

    testA = (np.abs(net.A) > 0).astype(float)
    mcls = matcls(testA, data.Y, data.P, np.eye(testA.shape[0]))
