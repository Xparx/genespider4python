import numpy as _np
import pandas as _pd
from sklearn.utils import check_array
from skgrni.utils import svd


def eta(X, i, norm=1):
    """
    Calculate the linear map of x_i on to X where
    x_i \in X for a set of i where i is over rows of X
    """

    X = check_array(X, accept_sparse=True)

    x = X[i, :].copy()
    Xtmp = _np.delete(X, (i), axis=0)
    projection = Xtmp.dot(x.T)

    if projection.ndim == 1 and norm == 'fro':
        norm = 2

    e = _np.linalg.norm(projection, norm)
    # e = _np.sum(abs(Xtmp.dot(x.T)))

    return e


def _alleta(X):
    """
    Calculate the linear map of x_i on to X where
    x_i \in X for each i where i is over rows of X
    """

    m = X.shape[0]

    etax = []
    for i in range(m):
        etax.append(eta(X, i))

    return etax


def _include(X):
    """
    Given linear overlap of x on X what samples can be estimated
    to overlap better than noise.
    """

    etax = _alleta(X)

    s = svd(X, compute_uv=False)
    s = s[-1]

    return etax >= s
