# Scikit-grni is a scientific toolkit focused on gene regulatory network inference solutions

This toolbox is built on on and is an **unofficial** repository for the python translated [GeneSPIDER toolbox](https://bitbucket.org/sonnhammergrni/genespider/src/master/).

This package is in alpha state and should be treated accordingly.


## Why skgrni?
Inference of gene regulatory networks (GRNs) is a central goal in systems biology.
It is therefore important to evaluate the accuracy of GRN inference methods in the light of network and data properties.
Although several packages are available for modelling, simulate, and analyse GRN inference, they offer limited evaluation and control of network topology together with system dynamics, experimental design, data properties, and noise characteristics.
Independent evaluation of these properties in simulations coupled with real world data collection is key to drawing conclusions about which inference method to use in a given condition and what performance to expect from it, as well as to obtain properties representative of real biological systems.


## Install

The package can be install through `pip`, requires python version >= 3.5.

    pip install skgrni

To work on the package or install from source one can clone the repository

    git clone git@gitlab.com:Xparx/scikit-grni.git
    cd scikit-grni
    pip install -e .
    
this requires that the python library directory is writeable by the user. Using a virtual environment or install for the current user is adviced.

## Basic usage

skikit-grni is takes it's inspiration from the excellent scikit-learn package and mimicks the structure when possible. A few modules ear currently availible.
    
    


### Who do I talk to?

* For questions contact [Andreas](mailto:andreas.tjarnberg@fripost.org)
* How to cite [bibtex]:

        @article{Tjarnberg2017-GeneSPIDER,
        author ="Tj\"arnberg, Andreas and Morgan, Daniel C. and Studham, Matthew  and Nordling, T\"orbjorn E. M. and Sonnhammer, Erik L. L.",
        title  ="GeneSPIDER - gene regulatory network inference benchmarking with controlled network and data properties",
        journal  ="Mol. BioSyst.",
        year  ="2017",
        pages  ="-",
        publisher  ="The Royal Society of Chemistry",
        doi  ="10.1039/C7MB00058H",
        url  ="http://dx.doi.org/10.1039/C7MB00058H",
        }

